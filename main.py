from requests import get
from bs4 import BeautifulSoup
import datetime
from datetime import date
from time import sleep, time
from random import randint
from warnings import warn
import smtplib, ssl, re, os

# ----------- SCRAPE PORTION -------------- #

datetime_group = []
title_group = []
link_group = []
hood_group = []
maptag_group = []

# create date var
today = date.today()
yesterday = today - datetime.timedelta(days = 1)

# get to the page of the antelope valley job posts
response = get('https://losangeles.craigslist.org/search/ant/fbh?sort=date&postal=93534&search_distance=20')

# create soup
html_soup = BeautifulSoup(response.text, 'html.parser')

sleep(randint(1,5))

# throw warning for status codes that are not 200
if response.status_code != 200:
    warn('Request: {}; Status code: {}'.format(requests, response.status_code))

# define the html text
html_soup = BeautifulSoup(response.text, 'html.parser')

# define the posts
posts = html_soup.find_all('li', class_='result-row')

# extract data by item
for post in posts:

    # post: time
    post_datetime = post.find('time', class_='result-date')['datetime']
    post_datetime_stripped = post_datetime.split()[0]

    if(post_datetime_stripped == str(today)):
        datetime_group.append(post_datetime_stripped)

        # post: title & link
        post_title = post.find('a', class_='result-title hdrlnk')
        post_title_text = post_title.text
        post_link = post_title['href']
        title_group.append(post_title_text)
        link_group.append(post_link)

        # post: neighborhood
        post_hood = post.find('span', class_='result-hood').text
        hood_group.append(post_hood)

        # post: maptag
        post_maptag = post.find('span', class_='maptag')
        if post_maptag != None:
            maptag_group.append(post_maptag.text)
        else:
            maptag_group.append("None")

# ----------- EMAIL PORTION -------------- #

port = 465
username = os.environ['MAIL_USERNAME']
password = os.environ['MAIL_PASSWORD']
sender_email = os.environ['MAIL_USERNAME']
receiver_email = os.environ['MAIL_USERNAME']



# create a secure ssl context
context = ssl.create_default_context()

with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
    i = 0
    message_group = ""
    while(i < (len(datetime_group))):
        message = """\

        Job Posting: i
        
        Time Posted: {}.
        Title: {}.
        Link: {}.
        Where: {}.
        Distance: {}.
        {}""".format(datetime_group[i], title_group[i], link_group[i],
                     hood_group[i], maptag_group[i], " ")
        message_group+=message
        i+=1

    server.login(username, password)
    server.sendmail(sender_email, receiver_email, message_group.encode("utf-8"))
